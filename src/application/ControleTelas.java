package application;

import javax.swing.JOptionPane;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import application.Metodo;

public class ControleTelas {

	@FXML private TextField login = new TextField();
	@FXML private TextField senha = new TextField();
	@FXML private TextField nomeDisciplina = new TextField();
	@FXML private TextField codigoDisciplina = new TextField();
	@FXML private TextField assuntoDisciplina = new TextField();
	@FXML private TextField searchDisciplina = new TextField();
	@FXML private TextField searchAssunto = new TextField();
	@FXML private TextArea enunciado = new TextArea();
	@FXML private ChoiceBox<String> disciplina = new ChoiceBox<String>();
	@FXML private ChoiceBox<String> assunto = new ChoiceBox<String>();
	@FXML private ChoiceBox<String> gabarito = new ChoiceBox<String>();
	@FXML private ChoiceBox<String> nivel = new ChoiceBox<String>();

	@FXML protected void entrarPrincipal(ActionEvent event){
		Telas.criarTela("/fxml/telaPrincipal.fxml");
	}
	@FXML protected void entrarDisciplinas(ActionEvent event){
		Telas.criarTela("/fxml/telaDisciplinas.fxml");
	}
	@FXML protected void entrarQuestoes(ActionEvent event){
	Telas.criarTela("/fxml/telaQuestoes.fxml");
	}
	@FXML protected void entrarBuscar(ActionEvent event){
		Telas.criarTela("/fxml/telaBuscar.fxml");
	}
	@FXML protected void popular(MouseEvent event){
		Metodo.popCombo(disciplina,nivel,gabarito);
		Metodo.popComboAssunto(assunto);
	}

	@FXML protected void logar(ActionEvent event){
		String user = login.getText();
		String pass = senha.getText();
		if(Metodo.autenticar(user,pass) == true){
			JOptionPane.showMessageDialog(null,"Login feito com sucesso!");
			Telas.criarTela("/fxml/telaPrincipal.fxml");
		} else{
			JOptionPane.showMessageDialog(null,"Login Invalido");
		}
	}
	@FXML protected void sair(ActionEvent event){
		int result = JOptionPane.showConfirmDialog(null, "Deseja sair?", "Aviso", JOptionPane.YES_NO_OPTION);
		Metodo.logout(result);
	}

	@FXML protected void pegar(ActionEvent event){
	//	Metodo.popComboAssunto(disciplina);
		//cadastrarQuestao();
		//Funcao para testes
	}

	@FXML protected void limparCampos(ActionEvent event){
		Metodo.limpar(nomeDisciplina,codigoDisciplina,assuntoDisciplina);
	}

	@FXML protected void cadastrarDisciplina(ActionEvent event){
		boolean status = Metodo.cadDisciplina(nomeDisciplina, codigoDisciplina);
		if (status == true){
			int result = JOptionPane.showConfirmDialog(null, "Deseja cadastrar outra disciplina?", "Aviso", JOptionPane.YES_NO_OPTION);
			if(result == 0){
				Metodo.limpar(nomeDisciplina, codigoDisciplina, assuntoDisciplina);
			} else{
				Telas.criarTela("/fxml/telaPrincipal.fxml");
			}
		}
	}

	@FXML protected void cadastrarAssunto(ActionEvent event){
		String assunto = assuntoDisciplina.getText();
		Metodo.cadAssunto(assunto);
		assuntoDisciplina.setText("");
	}

	@FXML protected void cadastrarQuestao(ActionEvent event){
		String disci = disciplina.getValue();
		String enun = enunciado.getText();
		String niv = nivel.getValue();
		String gab = gabarito.getValue();
		Metodo.cadQuestao(disci, enun,niv,gab);

	}

	@FXML protected void buscarQuestao(ActionEvent event){
		String sQuestao = searchDisciplina.getText();
		String sAssunto = searchAssunto.getText();
		Metodo.buscar(sQuestao,sAssunto);
	}


}
