package application;

import java.util.ArrayList;
import java.util.LinkedList;

public class Disciplinas {
	public String nomeDisciplina;
	public String codigoDisciplina;
	public ArrayList <String> assuntosDisciplina = new ArrayList<String>();
	public LinkedList <Questoes> questoesCadastradas = new LinkedList<Questoes>();
	public boolean status;


	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}


	public LinkedList<Questoes> getQuestoesCadastradas() {
		return questoesCadastradas;
	}
	public void setQuestoesCadastradas(LinkedList<Questoes> questoesCadastradas) {
		this.questoesCadastradas = questoesCadastradas;
	}

	/*@Override
	public String toString(){
	/

	}*/

	public String getNomeDisciplina() {
		return nomeDisciplina;
	}
	public void setNomeDisciplina(String nomeDisciplina) {
		this.nomeDisciplina = nomeDisciplina;
	}
	public String getCodigoDisciplina() {
		return codigoDisciplina;
	}
	public void setCodigoDisciplina(String codigoDisciplina) {
		this.codigoDisciplina = codigoDisciplina;
	}
	public ArrayList <String> getAssuntosDisciplina() {
		return assuntosDisciplina;
	}
	public void setAssuntosDisciplina(ArrayList <String> assuntosDisciplina) {
		this.assuntosDisciplina = assuntosDisciplina;
	}

}
