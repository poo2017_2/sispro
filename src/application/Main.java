package application;

import javafx.application.Application;
import javafx.stage.Stage;
import application.Telas;

public class Main extends Application {

	public void start(Stage palco){
		palco.setTitle("Gestao de Provas - SisPro.V-1.0.0");
		Telas.setPalco(palco);
		Telas.criarTela("/fxml/telaBuscar.fxml");
	}

	public static void main(String[] args) {
		launch(args);
	}
}
