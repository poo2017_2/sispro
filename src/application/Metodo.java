package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JOptionPane;
import application.Disciplinas;
import application.Questoes;

interface Metodo {

Disciplinas disciplina = new Disciplinas();

ObservableList<String> nivel =
FXCollections.observableArrayList(
    "1",
    "2",
    "3"
);

ObservableList<String> gabarito =
FXCollections.observableArrayList(
    "A",
    "B",
    "C",
    "D"
);

public static boolean autenticar(String login, String senha){
	if (login.equals("admin") && senha.equals("1234")){
		return true;
	} else{
		return false;
	}
}

public static void logout(int result){
	if (result == 0){
		System.out.println("Saindo");
		System.exit(1);
	}
}

public static boolean cadAssunto(String assunto){ //trocar pra boolean
	if (assunto.equals("")){
		JOptionPane.showMessageDialog(null,"Atencao! Assunto vazio!");
		return false;

	} else{
	disciplina.assuntosDisciplina.add(assunto);
	disciplina.status = true;
	JOptionPane.showMessageDialog(null,"Assunto cadastrado!");
	return true;
	}
}

public static boolean cadDisciplina(TextField nomeDisciplina, TextField codigoDisciplina){
	boolean flag_assunto = disciplina.status;
	boolean flag_ok = false;
	if (nomeDisciplina.getText().equals("") || codigoDisciplina.getText().equals("")){
		flag_ok = false;
	} else{
		flag_ok = true;
	}

	if (flag_assunto && flag_ok){
	ArrayList <String> assuntos = disciplina.getAssuntosDisciplina();
	disciplina.setNomeDisciplina(nomeDisciplina.getText());
	disciplina.setCodigoDisciplina(codigoDisciplina.getText());
	disciplina.setAssuntosDisciplina(assuntos);
	JOptionPane.showMessageDialog(null,"Disciplina Cadastrada!");
	System.out.println("STATUS_SUCCESS_1");
	return true;

	} else{
	JOptionPane.showMessageDialog(null,"Voce deixou algum campo vazio!");
	System.out.println("STATUS_FAIL_1");
	return false;
}
}

public static boolean cadQuestao(String nomeDisciplina, String enunciado, String nivel, String gabarito){
		Questoes questao = new Questoes();
		LinkedList <Questoes> listaQuestao = new LinkedList<Questoes>();
		questao.setNomeDisciplina(nomeDisciplina);
		questao.setEnunciado(enunciado);
		questao.setNivel(nivel);
		questao.setGabarito(gabarito);
		JOptionPane.showMessageDialog(null,"Questao Cadastrada!");
		listaQuestao.add(questao);
		disciplina.questoesCadastradas.addAll(listaQuestao);
	    System.out.println("Questoes cad:"+disciplina.getQuestoesCadastradas().toString());
		return true;
}


public static void limpar(TextField nomeDisciplina, TextField codigoDisciplina, TextField assuntoDisciplina){
	nomeDisciplina.setText("");
	codigoDisciplina.setText("");
	assuntoDisciplina.setText("");
}

public static void buscar(String sQuestao, String sAssunto) {
	System.out.println("Buscando termos: "+sQuestao+" e "+sAssunto);
}

public static void pegar() {
//	System.out.println("Buscando termos: "+sQuestao+" e "+sAssunto);
}

public static void popCombo(ChoiceBox <String> comboDisciplina,ChoiceBox<String> comboNivel, ChoiceBox<String> comboGabarito){
	String disci = disciplina.getNomeDisciplina();
	comboDisciplina.setItems(FXCollections.observableArrayList(
		    disci));
	comboDisciplina.setTooltip(new Tooltip("Selecione disciplina desejada!"));
	comboNivel.setItems(nivel);
	comboGabarito.setItems(gabarito);
}

public static void popComboAssunto(ChoiceBox<String> comboAssunto) {
	ArrayList<String> assu = disciplina.getAssuntosDisciplina();
	comboAssunto.setItems(FXCollections.observableArrayList(
		    assu));
	comboAssunto.setTooltip(new Tooltip("Selecione assunto desejado!"));
}

}