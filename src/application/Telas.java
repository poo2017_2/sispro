package application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Telas {
	private static Stage palco;
	public static void setPalco(Stage palcop){
		palco = palcop;
	}

	public static void criarTela(String local){
		try{
			FXMLLoader loader = new FXMLLoader(Main.class.getResource(local));
			Pane tela = loader.load();
			Scene scene = new Scene(tela);
			palco.setScene(scene);
			palco.setResizable(false);
			palco.show();
		} catch (Exception e){
			e.printStackTrace();
		}
	}


}
